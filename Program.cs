﻿using System;
using System.Collections.Generic;

namespace Lesson_4
{
    class MainClass
    {
        static void Main(string[] args)
        { 
            Player player_0 = new Player_Comander();
            Player player_1 = new Player_Healer();
            Player player_2 = new Player_Rifle();
            Player player_3 = new Player_Rifle_Sniper();
            
            
            player_0.ShowInfo("Stalker Command", "One", 35, 15, 100);
            Console.WriteLine("================================");
            player_1.ShowInfo("Stalker Healer", "Two", 21, 5, 100);
            Console.WriteLine("================================");
            player_2.ShowInfo("Stalker Rifle", "Three", 31, 11, 100);
            Console.WriteLine("================================");
            player_3.ShowInfo("Stalker Sniper", "Four", 42, 57, 100);
            Console.WriteLine("================================"); 
        }
    }

    class Weapon 
    {
        protected int DamageWeapon;
        protected int DistanceDamage;

        public void SetDamageWeapon(int DamageWeapon)
        {
            this.DamageWeapon = DamageWeapon;
        }

        public void SetDistanceDamage(int DistanceDamage)
        {
            this.DistanceDamage = DistanceDamage;
        }

        public virtual void ShowWeapon()
        {
            Console.WriteLine("-----------------");
            Console.WriteLine("Using Weapons:");
        }
    }

    class Knife : Weapon
    {
        private string knife;
        public Knife()
        {
            knife = "Knife:";
            DamageWeapon = 10;
            DistanceDamage = 1;
        }

        public override void ShowWeapon()
        {
            base.ShowWeapon();
            Console.WriteLine(knife);
            Console.WriteLine("Damage: " + DamageWeapon);
            Console.WriteLine("Distance: " + DistanceDamage);
        }
    }

    class Pistol : Weapon
    {
        private string pistol;

        public Pistol()
        {
            pistol = "Pistol:";
            DamageWeapon = 35;
            DistanceDamage = 30;
        }

        public override void ShowWeapon()
        {
            base.ShowWeapon();
            Console.WriteLine(pistol);
            Console.WriteLine("Damage: " + DamageWeapon);
            Console.WriteLine("Distance: " + DistanceDamage);
        }
    }

    class Rifle : Weapon
    {
        private string rifle;

        public Rifle()
        {
            rifle = "Rifle:";
            DamageWeapon = 50;
            DistanceDamage = 100;
        }

        public override void ShowWeapon()
        {
            base.ShowWeapon();
            Console.WriteLine(rifle);
            Console.WriteLine("Damage: " + DamageWeapon);
            Console.WriteLine("Distance: " + DistanceDamage);
        }
    }

    class Sniper_Rifle : Weapon
    {
        private string sniper_rifle;

        public Sniper_Rifle()
        {
            sniper_rifle = "Sniper Rifle:";
            DamageWeapon = 100;
            DistanceDamage = 1000;
        }
        public override void ShowWeapon()
        {
            base.ShowWeapon();
            Console.WriteLine(sniper_rifle);
            Console.WriteLine("Damage: " + DamageWeapon);
            Console.WriteLine("Distance: " + DistanceDamage);
        }
    }

    class Player
    {
       protected Weapon weapon1 = new Knife();
       protected Weapon weapon2 = new Pistol();
       protected Weapon weapon3 = new Rifle();
       protected Weapon weapon4 = new Sniper_Rifle();

        protected string name;
        protected string surname;
        protected int age;
        protected int killsenemy;
        protected int hp;

        public virtual void ShowInfo(string name, string surname, int age, int killsenemy, int hp)
        {
            this.name = name;
            this.surname = surname;
            this.age = age;
            this.killsenemy = killsenemy;
            this.hp = hp;

            Console.WriteLine("Name: " + name);
            Console.WriteLine("Surname: " + surname);
            Console.WriteLine("Age: " + age);
            Console.WriteLine("Kills Enemy: " + killsenemy);
            Console.WriteLine("Hp: " + hp);
        }
    }

    class Player_Comander : Player
    {
        public void SelectWeapon()
        {
            Console.WriteLine("================================");
            Console.WriteLine("Select weapon:\n" + "1.Sniper Rifle.\n");
            int select = Convert.ToInt32(Console.ReadLine());

            if (select == 1)
            {Console.WriteLine("================================");
                base.ShowInfo("Stalker Command", "One", 35, 15, 100);
                weapon1.ShowWeapon();
                weapon2.ShowWeapon();
                weapon3.ShowWeapon();
                weapon4.ShowWeapon();
            }
        }

        public override void ShowInfo(string name, string surname, int age, int killsenemy, int hp)
        {
            Player_Comander player_Comander = new Player_Comander();
            base.ShowInfo(name, surname, age, killsenemy, hp);
            weapon1.ShowWeapon();
            weapon2.ShowWeapon();
            weapon3.ShowWeapon();
            Console.WriteLine("================================");
            Console.WriteLine("You need more weapons?");
            string answer = Console.ReadLine();

            if (answer == "Yes" || answer == "yes")
            {
                player_Comander.SelectWeapon();
            }
        }
    }

    class Player_Healer : Player
    {
        public void SelectWeapon()
        {
            Console.WriteLine("================================");
            Console.WriteLine("Select weapon:\n" + "1.Rifle.\n" + "2.Sniper Rifle.\n");
            int select = Convert.ToInt32(Console.ReadLine());

            if (select == 1)
            {
                Console.WriteLine("================================");
                base.ShowInfo("Stalker Healer", "Two", 21, 5, 100);
                weapon1.ShowWeapon();
                weapon2.ShowWeapon();
                weapon3.ShowWeapon();
                Console.WriteLine("================================");
                Console.WriteLine("You need more weapons?");
                string answer = Console.ReadLine();

                if (answer == "Yes" || answer == "yes")
                {
                    Console.WriteLine("================================");
                    Console.WriteLine("Select weapon:\n" + "1.Sniper Rifle.\n");
                    int select1 = Convert.ToInt32(Console.ReadLine());

                    if (select1 == 1)
                    {Console.WriteLine("================================");
                        base.ShowInfo("Stalker Healer", "Two", 21, 5, 100);
                        weapon1.ShowWeapon();
                        weapon2.ShowWeapon();
                        weapon3.ShowWeapon();
                        weapon4.ShowWeapon();
                    }
                }
            }

            if (select == 2)
            {
                base.ShowInfo("Stalker Healer", "Two", 21, 5, 100);
                weapon1.ShowWeapon();
                weapon2.ShowWeapon();
                weapon4.ShowWeapon();
                Console.WriteLine("================================");
                Console.WriteLine("You need more weapons?");
                string answer = Console.ReadLine();

                if (answer == "Yes" || answer == "yes")
                {
                    Console.WriteLine("================================");
                    Console.WriteLine("Select weapon:\n" + "1.Rifle.\n");
                    int select1 = Convert.ToInt32(Console.ReadLine());

                    if (select1 == 1)
                    {Console.WriteLine("================================");
                        base.ShowInfo("Stalker Healer", "Two", 21, 5, 100);
                        weapon1.ShowWeapon();
                        weapon2.ShowWeapon();
                        weapon3.ShowWeapon();
                        weapon4.ShowWeapon();
                    }
                }
            }
        }
        public override void ShowInfo(string name, string surname, int age, int killsenemy, int hp)
        {
            Player_Healer player_Healer = new Player_Healer();
            base.ShowInfo(name, surname, age, killsenemy, hp);
            weapon1.ShowWeapon();
            weapon2.ShowWeapon();
            Console.WriteLine("================================");
            Console.WriteLine("You need more weapons?");
            string answer = Console.ReadLine();

            if (answer == "Yes" || answer == "yes")
            {
                player_Healer.SelectWeapon();
            }
        }
    }

    class Player_Rifle : Player
    {
        public void SelectWeapon()
        {
            Console.WriteLine("================================");
            Console.WriteLine("Select weapon:\n" + "1.Pistol.\n" + "2.Sniper Rifle.\n");
            int select = Convert.ToInt32(Console.ReadLine());

            if (select == 1)
            {
                Console.WriteLine("================================");
                base.ShowInfo("Stalker Rifle", "Three", 31, 11, 100);
                weapon1.ShowWeapon();
                weapon2.ShowWeapon();
                weapon3.ShowWeapon();
                Console.WriteLine("================================");
                Console.WriteLine("You need more weapons?");
                string answer = Console.ReadLine();

                if (answer == "Yes" || answer == "yes")
                {
                    Console.WriteLine("================================");
                    Console.WriteLine("Select weapon:\n" + "1.Sniper Rifle.\n");
                    int select1 = Convert.ToInt32(Console.ReadLine());

                    if (select1 == 1)
                    {
                        Console.WriteLine("================================");
                        base.ShowInfo("Stalker Rifle", "Three", 31, 11, 100);
                        weapon1.ShowWeapon();
                        weapon2.ShowWeapon();
                        weapon3.ShowWeapon();
                        weapon4.ShowWeapon();
                    }
                }
            }

            if (select == 2)
            {
                Console.WriteLine("================================");
                base.ShowInfo("Stalker Rifle", "Three", 31, 11, 100);
                weapon1.ShowWeapon();
                weapon3.ShowWeapon();
                weapon4.ShowWeapon();
                Console.WriteLine("================================");
                Console.WriteLine("You need more weapons?");
                string answer = Console.ReadLine();

                if (answer == "Yes" || answer == "yes")
                {
                    Console.WriteLine("================================");
                    Console.WriteLine("Select weapon:\n" + "1.Pistol.\n");
                    int select1 = Convert.ToInt32(Console.ReadLine());

                    if (select1 == 1)
                    {
                        Console.WriteLine("================================");
                        base.ShowInfo("Stalker Rifle", "Three", 31, 11, 100);
                        weapon1.ShowWeapon();
                        weapon2.ShowWeapon();
                        weapon3.ShowWeapon();
                        weapon4.ShowWeapon();
                    }
                }
            }
        }
        public override void ShowInfo(string name, string surname, int age, int killsenemy, int hp)
        {
            Player_Rifle player_Rifle = new Player_Rifle();
            base.ShowInfo(name, surname, age, killsenemy, hp);
            weapon1.ShowWeapon();
            weapon3.ShowWeapon();
            Console.WriteLine("================================");
            Console.WriteLine("You need more weapons?");
            string answer = Console.ReadLine();

            if (answer == "Yes" || answer == "yes")
            {
                player_Rifle.SelectWeapon();
            }
        }
    }

    class Player_Rifle_Sniper : Player
    {
        public void SelectWeapon()
        {
            Console.WriteLine("================================");
            Console.WriteLine("Select weapon:\n" + "1.Knife.\n" + "2.Rifle.\n");
            int select = Convert.ToInt32(Console.ReadLine());

            if (select == 1)
            {
                Console.WriteLine("================================");
                base.ShowInfo("Stalker Sniper", "Four", 42, 57, 100);
                weapon1.ShowWeapon();
                weapon2.ShowWeapon();
                weapon4.ShowWeapon();
                Console.WriteLine("================================");
                Console.WriteLine("You need more weapons?");
                string answer = Console.ReadLine();

                if (answer == "Yes" || answer == "yes")
                {
                    Console.WriteLine("================================");
                    Console.WriteLine("Select weapon:\n" + "1.Rifle.\n");
                    int select1 = Convert.ToInt32(Console.ReadLine());

                    if (select1 == 1)
                    {
                        Console.WriteLine("================================");
                        base.ShowInfo("Stalker Sniper", "Four", 42, 57, 100);
                        weapon1.ShowWeapon();
                        weapon2.ShowWeapon();
                        weapon3.ShowWeapon();
                        weapon4.ShowWeapon();
                    }
                }
            }

            if (select == 2)
            {
                Console.WriteLine("================================");
                base.ShowInfo("Stalker Sniper", "Four", 42, 57, 100);
                weapon2.ShowWeapon();
                weapon3.ShowWeapon();
                weapon4.ShowWeapon();
                Console.WriteLine("================================");
                Console.WriteLine("You need more weapons?");
                string answer = Console.ReadLine();

                if (answer == "Yes" || answer == "yes")
                {
                    Console.WriteLine("================================");
                    Console.WriteLine("Select weapon:\n" + "1.Knife.\n");
                    int select1 = Convert.ToInt32(Console.ReadLine());

                    if (select1 == 1)
                    {
                        Console.WriteLine("================================");
                        base.ShowInfo("Stalker Sniper", "Four", 42, 57, 100);
                        weapon1.ShowWeapon();
                        weapon2.ShowWeapon();
                        weapon3.ShowWeapon();
                        weapon4.ShowWeapon();
                    }
                }
            }
        }

        public override void ShowInfo(string name, string surname, int age, int killsenemy, int hp)
        {
            Player_Rifle_Sniper player_Rifle_Sniper = new Player_Rifle_Sniper();
            base.ShowInfo(name, surname, age, killsenemy, hp);
            weapon2.ShowWeapon();
            weapon4.ShowWeapon();
            Console.WriteLine("================================");
            Console.WriteLine("You need more weapons?");
            string answer = Console.ReadLine();

            if (answer == "Yes" || answer == "yes")
            {
                player_Rifle_Sniper.SelectWeapon();
            }
        }
    }
}
